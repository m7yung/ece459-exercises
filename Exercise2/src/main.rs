// You should implement the following function

//0 1 2 3 4 5
//0 1 1 2 3 5 
fn fibonacci_number(n: u32) -> u32 {
    if n <=1 {
        return n
    }
    if n > 0{
        return fibonacci_number(n -1) + fibonacci_number(n-2)
    }
}


fn main() {
    println!("{}", fibonacci_number(10));
}
