// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    //multiples of 3 are 3 6 9 12... 
    println!("multiple1: {}", multiple1);
    
    let mut c1 = 0;
    let mut c2 = 0;

    for n in 1..number{
        if n % multiple1 == 0{
            c1+=n;
            //println!("multiple of 5: {}", n);
        } 
        else if n % multiple2 == 0 {
            c2+=n;
            //println!("multiple of 3: {}", n);
        }
    }
    return c1 + c2;
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
